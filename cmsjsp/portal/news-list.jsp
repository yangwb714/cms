<%@ page import="com.fasterxml.jackson.annotation.JsonInclude.Include"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
	<%@include file="include/head.jsp"%>
  </head>  
  <body>
      <%@include file="include/nav.jsp"%>
      <div class="container-fluid">
            <div class="container mt-15">
                <div class="notice-page-left">

                    <div class="notice-page-search">
                        <div class="notice-page-title">
                            新闻列表
                        </div>
                        <div class="notice-page-form">
                            <input type="button" onClick="search()" class="notice-page-button bg-d1e2f6" value="查询">
                            <input type="text" id="keyword" class="notice-page-echo">
                        </div>
                        <c:if test="${status=='1'}">
                        <div class="notice-page-tag">
                            <a href="JavaScript:drafts()">我的草稿</a>
                        </div>
                        </c:if>
                        <c:if test="${status=='0'}">
                        <div class="notice-page-tag notice-page-tag-draft">
                            <a href="JavaScript:drafts()">我的草稿</a>
                        </div>
                        </c:if>
                    </div>
        
                    <ul class="news-page-content">
                    	<c:forEach items="${newsPage.list}" var="news" varStatus="status">
                        <li>
                            <a href="${ctx}/news/view/${news.id}" class="news-page-img">
                                <img src="${news.cover}" alt="${news.title}">
                            </a>
                            <div class="news-page-nr">
                                <p class="news-page-nr-p1 news_html_content">                              	
                                	<c:out value="${news.content}" escapeXml="true"/>  
                                </p>
                                <p class="news-page-nr-p2">
                                	<fmt:formatDate value="${news.createDate}" pattern="yyyy.MM.dd HH:mm"/>
                                </p>
                            </div>                           
                        </li>
                        </c:forEach>  
                    </ul>                
                </div>
                <div class="notice-page-right">
                    <%@include file="include/todo.jsp"%>
                    <div class="matters-concerned mt-20">
                        <%@include file="include/apps.jsp"%> 
                    </div>
                </div>
            </div>
      </div>
    

      <%@include file="include/foot.jsp"%>  
      <script>
      	$(function(){
      		var x = document.getElementsByClassName("news_html_content");
    		for (var i = 0; i < x.length; i++) {
    			setContent(x[i]);
    			//alert(x[i]);
    		}     		
    	});
      	
      	function search(){
    		var keyword = $("#keyword").val();
			location = "${ctx}/news/list?title="+keyword;
		}  
		
		function drafts(){
			var keyword = $("#keyword").val();
			location = "${ctx}/news/list?title="+keyword+"&status=0";
		}
		//过滤格式
		function setContent(obj) {
			//alert(obj.innerHTML);
			obj.innerHTML = obj.innerHTML.replace(/&lt;[^>].*?&gt;/g,''); //去除HTML tag
			obj.innerHTML = obj.innerHTML.replace(/nbsp;/g,'');  //去除  ；
			obj.innerHTML = obj.innerHTML.replace(/&amp;/g,'');  //去除  ；
			obj.innerHTML = obj.innerHTML.replace(/\s/g,'');  //去除 空格
			obj.innerHTML = obj.innerHTML.replace(/middot;/g,'');  //去除 空格
			obj.innerHTML = obj.innerHTML.replace(/rdquo;/g,'');  //去除 空格
			obj.innerHTML = obj.innerHTML.replace(/ldquo;/g,'');  //去除 空格
			   
			var len=obj.innerHTML.length;
			if(len>100){
			 	obj.innerHTML = obj.innerHTML.substring(0,100)+"...";
			}
		}
				
      </script>   
  </body>
</html>
