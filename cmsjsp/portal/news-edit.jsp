<%@ page contentType="text/html;charset=UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
	<%@include file="include/head.jsp"%>
	<link rel="stylesheet" type="text/css" href="${ctxStatic}/sec/${site.theme}/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="${ctxStatic}/sec/${site.theme}/css/wangEditor-1.1.0-min.css">
  </head>  
  <body style="background-color: #f8f8f8;">
      <%@include file="include/nav.jsp"%>
      <div class="container-fluid" >
            <div class="container mt-10 notice-list-edit-title" style="background-color: #fff;">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;公告编辑
            </div>
            <div class="container news-list-detailed-bg" style="margin-top: 0px;">
                <div class="notice-list-edit">
                    <input type="text" placeholder="填写标题" class="notice-list-edit-inputtitle">
                    <div id='txtDiv' style='border:1px solid #cccccc; height:240px;'>
                </div>
                <div class="notice-list-edit-fj">
                    附件：<a href="">附件添加</a> 
                </div>
                <div class="notice-list-edit-mm">
                    密级：
                    <select name="" id="">
                        <option value="1">一级</option>
                    </select>
                </div>
                <div class="notice-list-edit-mm" style="text-align: left">
                    <span>发布人：xxxxxx</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;      
                    <span>发布部门：工程部</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     
                    <span>发布时间：2019.07.22</span> 
                      
                </div>
                <div class="notice-list-edit-submit mt-20" style="text-align: center;float:left;width: 100%;padding-bottom:20px;">
                    <input type="button" value="取消" style="background-color:#e5e5e5;border: 0px;width: 80px;height: 30px;text-align: center;line-height: 30px;">
                    <input type="button" value="存为草稿" style="background-color:#e5e5e5;border: 0px;width: 80px;height: 30px;text-align: center;line-height: 30px;;">
                    <input type="button" value="发布" style="background-color:#f85858;border: 0px;width: 80px;height: 30px;text-align: center;line-height: 30px;color: #fff;">
                </div>
            </div>
      </div>
    

      <%@include file="include/foot.jsp"%> 
      <script src="${ctxStatic}/sec/${site.theme}/js/jquery-1.10.2.min.js" type="text/javascript"></script>
      <script type="text/javascript" src='${ctxStatic}/sec/${site.theme}/js/wangEditor-1.1.0-min.js'></script>
      <script> 
            var editor = $('#txtDiv').wangEditor({});
      </script>
  </body>
</html>
