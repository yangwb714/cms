<%@ page contentType="text/html;charset=UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
	<%@include file="include/head.jsp"%>
  </head>  
  <body style="background-color: #f8f8f8;">
      <%@include file="include/nav.jsp"%>
      <div class="container-fluid" >
            <div class="container news-list-detailed-bg">
                <div class="news-list-detailed-update">
                    <a href="${ctx}/notify/view/${notifyId}?edit=1" target="not"">编辑修改</a>
                </div>
                <h3 class="notice-list-detailed-title">
                ${notifyTitle}
                </h3>
                <div class="notice-list-detailed-content">
                ${notifyContent}
                </div>
                <div class="notice-list-detailed-time">
                    <p>${notifyDept}</p>
                    <p><fmt:formatDate value="${notifyUpdate}" pattern="yyyy.MM.dd"/></p>
                </div>
            </div>
      </div>
    

      <%@include file="include/foot.jsp"%>
      <script></script>   
  </body>
</html>
