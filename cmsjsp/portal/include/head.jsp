<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="taglib.jsp"%>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta charset="utf-8">
<title>十七所门户</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content=""> 
<link href="${ctxStatic}/sec/${site.theme}/css/bootstrap.min.css" rel="stylesheet">
<link href="${ctxStatic}/sec/${site.theme}/css/bootstrap-responsive.min.css" rel="stylesheet">
<link rel="stylesheet" href="${ctxStatic}/sec/${site.theme}/css/style.css?6">
<link rel="stylesheet" href="${ctxStatic}/sec/${site.theme}/css/comm.css">
<script src="${ctxStatic}/sec/${site.theme}/js/jquery.js"></script>
<!--[if IE 6]>
<script src="js/DD_belatedPNG_0.0.8a-min.js" type="text/javascript"></script>
<script type="text/javascript">DD_belatedPNG.fix('*');</script>
<![endif]-->
