<%@ page import="com.fasterxml.jackson.annotation.JsonInclude.Include"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<div class="container-fluid banner">
    <div class="container nav-fa1" style="margin-top: 142px;">
          <ul class="nav-fa1-list">
          	<c:forEach items="${navList}" var="nav" varStatus="status">               		
           	<li <c:if test="${status.first}">class="li1"</c:if>>
                   <a href="${nav.outlink}" target="nav">${nav.title}</a>
            </li>
          	</c:forEach>
              <li class="username">
                  欢迎：${myName }
              </li>
              <li class="notice">
                 <a href="${ctx}/publish" class="relative">
                     <img src="${ctxStatic}/sec/${site.theme}/img/notice-icon.png" alt="" class="absolute" width="30" height="30">
                     发布公告
                 </a> 
              </li>
          </ul>
    </div>
</div>