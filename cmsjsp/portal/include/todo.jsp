<%@ page contentType="text/html;charset=UTF-8"%>
<div class="matters-concerned matters-bg">
  <div class="lanmu-bg relative">
      <i class="matters-concerned-icon absolute"></i>
      代办事宜
  </div>
  <ul class="matters-concerned-ul matters-width">
  	<c:forEach items="${todos}" var="todo" varStatus="status">
  		<li class="border-dashed-1 relative">
  			<i class="dian absolute"></i>
  			<a href="${ctx}${todo.url}" target="todo">${todo.title}</a>
  		</li>
  	</c:forEach>                       
  </ul>
  <div style="width: 100%;height:220px;"></div>
</div>