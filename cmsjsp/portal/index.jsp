<%@ page import="com.fasterxml.jackson.annotation.JsonInclude.Include"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <%@include file="include/head.jsp"%>
  </head>
  <body>
      <%@include file="include/nav.jsp"%>
      <div class="container-fluid">
            <div class="container mt-15">
                <%@include file="include/todo.jsp"%>
                <div class="notice-lm ml-15">
                    <div class="notice-lm-bg relative">
                        <i class="notice-lm-icon absolute"></i>
                        <p class="notice-lm-title">电子公告</p> 
                        <p class="notice-lm-sub">Electronic Bulletin</p> 
                        <a href="${ctx}/notify/list" class="absolute notice-lm-gd">更多>></a>
                    </div>
                    <ul class="notice-lm-ul">
                    	<c:forEach items="${notifyPage.list}" var="notify">
                    	<li>
                            <a href="${ctx}/notify/view/${notify.id}" target="not" class="notice-lm-tag">【${notify.department}】</a>
                            <a href="${ctx}/notify/view/${notify.id}">${notify.title}</a>
                            <span class="fr">
                            	<fmt:formatDate value="${notify.createDate}" pattern="MM.dd" />
                            </span>
                        </li>
                    	</c:forEach>                   
                    </ul>
                </div>
                <div class="moral-integrity">
                    <div class="calendar-w">
                        <div class="calendar-title">
                            <i class="calendar-icon"></i>
                            	万年历
                        </div>
                        <div class="calendar-info"> 
                            <iframe src="${ctxStatic}/sec/${site.theme}/rili2.html" frameborder="0" width="540" height="530" scrolling="no"></iframe>
                        </div>
                    </div>
                </div>
            </div>
      </div>
      <div class="container-fluid">
            <div class="container mt-15">
            	<c:forEach items="${newsPage.list}" var="news"  varStatus="status"> 
            		<c:if test="${status.index<4}">
	            		<div>
	            			<a href="${ctx}/news/view/${news.id}" target="news">
	            			<img class="banner1" src="${news.cover}" alt="">
	            			</a>
	            		</div>
            		</c:if>
               	</c:forEach>
               	<!-- 
                <div class="banner1 fl mr-20"><img src="img/test.jpg" alt="" width="100%"></div>
                <div class="banner1 fl"><img src="img/test.jpg" alt="" width="100%"></div>
                <div class="banner1 fr ml-20"><img src="img/test.jpg" alt="" width="100%"></div>
                <div class="banner1 ml-20 fr"><img src="img/test.jpg" alt="" width="100%"></div>
                 -->
            </div>
      </div>      
      <div class="container-fluid">
            <div class="container mt-15">
                <div class="matters-concerned">
                   <%@include file="include/apps.jsp"%> 
                </div>
                <div class="right-box">
                    <div class="right-box">
                        <div class="corporate-culture">
                            <div class="lanmu-bg relative">
                                <i class="corporate-culture-icon absolute"></i>
                                	${actName }
                            </div>
                            <ul class="matters-concerned-ul corporate-width">
                                <c:forEach items="${actPage.list}" var="act">
		                    	<li class="relative">
                                    <i class="dian absolute"></i>
                                    <a href="${act.outlink}" target="act">${act.title }</a>
                                </li>
		                    	</c:forEach> 
                            </ul>
                        </div>
                        <div class="operational-resources">
                            <div class="lanmu-bg relative">
                                <i class="operational-resources-icon absolute"></i>
                                ${resName }
                            </div>
                            <ul class="matters-concerned-ul corporate-width">
		                    	<c:forEach items="${resPage.list}" var="res">
		                    	<li class="relative">
                                    <i class="dian absolute"></i>
                                    <a href="${res.outlink}" target="res">${res.title }</a>
                                </li>
		                    	</c:forEach>                              
                            </ul>
                        </div>
                    </div>
                    <div class="department mt-20">
                        <div class="lanmu-bg relative">
                            <i class="department-icon absolute"></i>
                            ${deptName }
                        </div>
                        <ul class="department-ul">
                        	<c:forEach items="${deptList}" var="dept">
		                    <li class="department-icon-1">
                                <a href="${dept.outlink}" target="dept">
                                   	${dept.title}
                                </a>
                            </li>
		                    </c:forEach> 
                        </ul>
                    </div>
                </div>
            </div>
      </div>
      <div class="container-fluid">
            <div class="container mt-15">
            	<c:forEach items="${otherList}" var="other">
            	<a href="${other.outlink}">
            		<img src="${other.cover}" alt="">
            	</a>
                </c:forEach> 
            </div>
      </div>
      <%@include file="include/foot.jsp"%>     
  </body>
</html>
