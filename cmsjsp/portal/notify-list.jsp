<%@ page contentType="text/html;charset=UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
     <%@include file="include/head.jsp"%>
  </head>  
  <body>
      <%@include file="include/nav.jsp"%>
      <div class="container-fluid">
            <div class="container mt-15">
                <div class="notice-page-left">

                    <div class="notice-page-search">
                        <div class="notice-page-title">
                           	 公告列表
                        </div>
                        <div class="notice-page-form">
                            <input type="button" onClick="search()" class="notice-page-button bg-d1e2f6" value="查询">
                            <input type="text" id="keyword" class="notice-page-echo">
                        </div>
                        <c:if test="${status=='1'}">
                        <div class="notice-page-tag">
                            <a href="JavaScript:drafts()">我的草稿</a>
                        </div>
                        </c:if>
                        <c:if test="${status=='0'}">
                        <div class="notice-page-tag notice-page-tag-draft">
                            <a href="JavaScript:drafts()">我的草稿</a>
                        </div>
                        </c:if>
                    </div>

               
                    <ul class="notice-page-content">
                    	<c:forEach items="${notifyPage.list}" var="notify">
                        <li>
                            <a href="${ctx}/notify/view/${notify.id}" target="not"" class="notice-page-nr"><span>[${notify.department}]</span>&nbsp;&nbsp;${notify.title}</a>
                            <a href="" class="notice-page-time"><fmt:formatDate value="${notify.createDate}" pattern="yyyy.MM.dd" /></a>
                        </li>
                        </c:forEach>                     
                    </ul> 
                                      
                </div>
                <div class="notice-page-right">
                     <%@include file="include/todo.jsp"%>
                    <div class="matters-concerned mt-20">
                        <%@include file="include/apps.jsp"%> 
                    </div>
                </div>
            </div>
      </div>
    

      <%@include file="include/foot.jsp"%>   
      <script>
      	$(function(){
			 	
      	
      	});
      	function search(){
      		var keyword = $("#keyword").val();
			location = "${ctx}/notify/list?title="+keyword;
		}  
		
		function drafts(){
			var keyword = $("#keyword").val();
			location = "${ctx}/notify/list?title="+keyword+"&status=0";
		}
      </script>   
  </body>
</html>
